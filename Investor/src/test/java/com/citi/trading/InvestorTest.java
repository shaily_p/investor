package com.citi.trading;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;


public class InvestorTest {
	
	public static class MockMarket implements OrderPlacer{
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			callback.accept(order);
		}
	}
	
	@Test
	public void testinvestorPortfolio_cash1() {
		OrderPlacer mark = new MockMarket();
		
		Investor invest = new Investor(20000,mark);
		invest.buy("GOOG", 200, 60); //Adding a stock to the portfolio
		invest.buy("MRK", 100, 60); //Adding another stock to the portfolio
		//Test if both the stocks are added to the portfolio 
		assertThat(invest.getPortfolio(), both(hasEntry("GOOG",200)).and(hasEntry("MRK",100)));
		assertThat(invest.getCash(), closeTo(2000.0,0.00001));
}

	@Test
	public void testinvestorCash_portfolio2() {
		OrderPlacer mark = new MockMarket();
		
		Investor invest = new Investor(20000,mark);
		invest.buy("MRK", 200, 60);
		invest.getCash();
		assertThat(invest.getCash(),closeTo(8000.0,0.00001));
		assertThat(invest.getPortfolio(),hasEntry("MRK",200));
		

}

}
